# Pixel Art
Example application written in JavaScript. Draw image pixel by pixel.

## Getting started
Install all dependencies listed in package.json using npm.
```
    npm i -D {dependency}
```

### Bundle js code
```
    npm run start
```

### Running tests
```
    npm run test 
```
 or 
 ```
    ./node_modules/.bin/nightwatch
```

### Author
* **Sotnichenko Roman** (Telegram: @RomanSo)

### License
This project is licensed under the MIT License.


