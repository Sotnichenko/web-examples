class Clock {
    oneSecond = () => 1000;
    getCurrentTime = () => new Date();
    clear = () => console.clear();
    log = message => console.log(message);

    compose = (...fns) => arg => fns.reduce((composed, f) => f(composed), arg);

    serializeClockTime = date => ({
        hours: date.getHours(),
        minutes: date.getMinutes(),
        seconds: date.getSeconds()
    });

    civilianHours = clockTime => ({
        ...clockTime,
        hours: (clockTime.hours > 12)
            ? clockTime.hours - 12
            : clockTime.hours
    });

    appendAMPM = clockTime => ({
        ...clockTime,
        ampm: (clockTime.hours >= 12) ? 'PM' : 'AM'
    });

    display = target => time => target(time);

    formatClock = format =>
        time => format
            .replace('hh', time.hours)
            .replace('mm', time.minutes)
            .replace('ss', time.seconds)
            .replace('tt', time.ampm);

    prependZero = key => clockTime => ({
        ...clockTime,
        [key]: (clockTime[key] < 10)
            ? '0' + clockTime[key]
            : clockTime[key]
    });

    convertToCivilianTime = clockTime =>
        compose(
            this.appendAMPM,
            this.civilianHours
        )(clockTime);

    doubleDigits = civilianTime => this.compose(
        this.prependZero('hours'),
        this.prependZero('minutes'),
        this.prependZero('seconds')
    )(civilianTime);

    startTicking = () => setInterval(
        compose(
            clear,
            this.getCurrentTime,
            this.serializeClockTime,
            this.convertToCivilianTime,
            this.doubleDigits,
            this.formatClock('hh:mm:ss tt'),
            display(log)
        ),
        this.oneSecond
    )
}

const cl = new Clock();
cl.startTicking();