const x = require("./some_func");
const { print, log } = require('./some_func');
const react = require('react');

const getFakeMembers = count => new Promise((resolves, rejects) => {
    const api = `https://api.randomuser.me/?nat=US&results=${count}`;
    const request = new XMLHttpRequest();
    request.open('GET', api);
    request.onload = () => {
        request.status === 200
            ? resolves(JSON.parse(request.response).results)
            : reject(Error(request.statusText));
        request.onerror = err => rejects(err);
        request.send();
    }
});

x.print('printing a message');
x.log('logging a message', new Date());
x.log(react, new Date());

var createScream = function(logger) {
    return function(message) {
        logger(message.toUpperCase() + '!!!');
    }
};
const createScream2 = logger => message => logger(message.toUpperCase() + '!!!');
const scream = createScream2(message => console.log(message));
scream('functions can be returned from other functions');

let colorLawn = {
    title: 'lawn',
    color: '#0F0',
    rating: 9
};

const rateColor = (color, rating) => Object.assign({}, color, {rating: rating});
const rateColor2 = (color, rating) => ({...color, rating});
console.log(rateColor2(colorLawn, 5).rating);
console.log(colorLawn.rating);

const colors = [
    {
        id: '-xekare',
        title: 'rad red',
        rating: 3
    },
    {
        id: '-jbwsof',
        title: 'big blue',
        rating: 2
    },
    {
        id: '-prigbj',
        title: 'grizzly grey',
        rating: 5
    },
    {
        id: '-ryhbhsl',
        title: 'banana',
        rating: 1
    }
];

const hashColors = colors.reduce(
    (hash, {id, title, rating}) => {
        hash[id] = {title, rating};
        return hash;
    },
    {}
);

console.log(hashColors);

const countdown = (value, fn, delay = 1000) => {
    fn(value);
    return value > 0
        ? setTimeout(() => countdown(value - 1, fn, delay), delay)
        : value;
};

countdown(10, console.log, 500);

var ray = {
    type: "person",
    data: {
        gender: "male",
        info: {
            id: 22,
            fullname: {
                first: "Ray",
                last: "Deacon"
            }
        }
    }
};

const deepPick = (fields, object = {}) => {
    const [first, ...remaining] = fields.split(".");
    return (remaining.length)
        ? deepPick(remaining.join("."), object[first])
        : object[first];
};

console.log(deepPick("type", ray));
console.log(deepPick("data", ray));
console.log(deepPick("data.info.fullname.first", ray));

const compose = (...fns) => (arg) => fns.reduce((composed, f) => f(composed), arg);
const template = "hh:mm:ss tt";
const replaceValues = compose(
    String.prototype.replace,
    String.prototype.replace
);

console.log(replaceValues(template));



