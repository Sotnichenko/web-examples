const webpack = require('webpack');

const HtmlWebPackPlugin = require("html-webpack-plugin");
const htmlPlugin = new HtmlWebPackPlugin({
    template: './public/index.html',
    filename: './index.html'
});

module.exports = {
    entry: {
        entry: __dirname + '/src/js/index.js'
    },
    output: {
        filename: 'bundle.js',
        sourceMapFilename: 'bundle.map'
    },
    devtool: "#source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    plugins: [htmlPlugin]
};
