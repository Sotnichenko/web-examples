const SELENIUM_CONFIGURATION = {
    start_process: true,
    server_path: 'bin/selenium-server-standalone-3.9.1.jar',
    host: '127.0.0.1',
    port: 4444,
    cli_args: {
        "webdriver.gecko.driver": "./bin/geckodriver.exe",
        "wibdriver.chrome.driver": "./bin/chromedriver"
    }
};

const FIREFOX_CONFIGURATION = {
    browserName: 'firefox',
    javascriptEnabled: true,
    acceptSslCerts: true
};

const CHROME_CONFIGURATION = {
    browserName: 'chrome'   ,
    javascriptEnabled: true,
    acceptSslCerts: true
};

const DEFAULT_CONFIGURATION = {
    launch_url: 'http://localhost',
    selenium_port: 4444,
    selenium_host: 'localhost',
    desiredCapabilities: FIREFOX_CONFIGURATION
};

const ENVIRONMENTS = {
    default: DEFAULT_CONFIGURATION
};

module.exports = {
    src_folders: ['src/tests'],
    selenium: SELENIUM_CONFIGURATION,
    test_settings: ENVIRONMENTS
};
