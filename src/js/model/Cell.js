
class Cell {

    constructor(x, y, size = 10, color = "#FFF") {
        this._x = x;
        this._y = y;
        this._size = size;
        this._color = color;
    }

    get x() {
        return this._x;
    }

    set x(value) {
        this._x = value;
    }

    get y() {
        return this._y;
    }

    set y(value) {
        this._y = value;
    }

    get size() {
        return this._size;
    }

    set size(value) {
        this._size = value;
    }

    get color() {
        return this._color;
    }

    set color(value) {
        this._color = value;
    }
}

export default Cell;
