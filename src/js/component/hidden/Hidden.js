import React from 'react';

const Hidden = () => <h1>Congratulations!<br/>You've found a hidden page!</h1>;

export default Hidden;

