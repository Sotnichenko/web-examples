import React from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Main from './main/Main';
import Easel from './pixel-easel/Easel';
import Menu from './menu/Menu';
import Hidden from './hidden/Hidden';
import Summary from "./summary/Summary";
import AddColorFormFunctional from "./add-color-form/AddColorFormFunctional";
import StarRating from "./star-rating/StarRating";
import Ajax from "./ajax/Ajax";

import 'jquery';
import 'popper.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../css/component/app.css';

const listItemOnClick = (event) => {
    $(event.target).toggleClass("active");
};

const App = () => (
    <Router>
        <div>
            <ul className="list-group">
                <li className="list-group-item active">
                    <Link to="/">Main</Link>
                </li>
                <li className="list-group-item">
                    <Link to="/easel">Easel</Link>
                </li>
                <li className="list-group-item">
                    <Link to="/star-rating">Star rating</Link>
                </li>
                <li className="list-group-item">
                    <Link to="/menu">Menu</Link>
                </li>
                <li className="list-group-item">
                    <Link to="/summary">Summary</Link>
                </li>
                <li className="list-group-item">
                    <Link to="/add-color-form">Add color form</Link>
                </li>
                <li className="list-group-item">
                    <Link to="/ajax">Ajax</Link>
                </li>
            </ul>
            <hr />
            <Route exact path="/" component={Main} />
            <Route exact path="/easel" component={Easel} />
            <Route exact path="/star-rating" component={StarRating} />
            <Route exact path="/menu" component={Menu} />
            <Route exact path="/hidden" component={Hidden} />
            <Route exact path="/summary" component={Summary} />
            <Route exact path="/add-color-form" component={AddColorFormFunctional} />
            <Route exact path="/ajax" component={Ajax} />
        </div>
    </Router>
);

export default App;
