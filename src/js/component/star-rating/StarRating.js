import React, {Component} from 'react';
import PropType from 'prop-types';
import Star from './Star';

class StarRating extends Component {

    constructor(props) {
        super(props);
        this.state = {
            starsSelected: props.starsSelected || 0
        }
    }

    static propTypes = {
        totalStars: PropType.number
    };

    static defaultProps = {
        totalStars: 5
    };

    change = starsSelected => this.setState({starsSelected});

    render() {
        console.log("================");
        const {totalStars} = this.props;
        const {starsSelected} = this.state;
        return (
            <div className="star-rating">
                {[...Array(totalStars)].map((n, i) => {
                    console.log("n: " + n + ", i: " + i);
                        return (
                            <Star key={i}
                                  selected={i < starsSelected}
                                  onClick={() => this.change(i + 1)}
                            />
                        )
                    }
                )}
                <p>{starsSelected} of {totalStars} stars</p>
            </div>
        );
    }
}

export default StarRating;