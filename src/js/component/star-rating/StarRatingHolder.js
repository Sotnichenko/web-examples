import React, {Component} from 'react';
import AddColorForm from "../add-color-form/AddColorForm";
import ColorList from "../add-color-form/ColorList";

class StarRatingHolder extends Component {

    constructor(props) {
        super(props);
        this.state = {
            colors: []
        }
    }

    render() {
        const {colors} = this.state;
        return (
            <div className="star-rating-holder">
                <AddColorForm/>
                <ColorList colors={colors}/>
            </div>
        );
    }
}

export default StarRatingHolder;