import React from 'react';
import PropTypes from 'prop-types';
import createReactClass from 'create-react-class';

// ES5 style Summary
const Summary = createReactClass({

    displayName: "Summary",

    propTypes: {
        ingredients: PropTypes.number.isRequired,
        steps: PropTypes.number.isRequired,
        title: (props, propName) =>
            (typeof props[propName] !== 'string')
                ? new Error("A title must be a string")
                : (props[propName].length > 20)
                    ? new Error('title is over 20 characters')
                    : null
    },

    getDefaultProps() {
        return {
            ingredients: 0,
            steps: 0,
            title: "[Recipe Title]"
        }
    },

    render() {
        const {ingredients, steps, title}  = this.props;
        return (
            <div className="summary">
                <h1>{title}</h1>
                <p>
                    <span>{ingredients} Ingredients | </span>
                    <span>{steps} Steps</span>
                </p>
            </div>
        );
    }
});

export default Summary;
