import React, {Component} from 'react';
import PropTypes from 'prop-types';

class AddColorForm extends Component {

    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
    }

    static propTypes = {
        onNewColor: PropTypes.func.isRequired
    };

    static defaultProps = {
        onNewColor: (title, color) => console.log(`New color: ${title} | ${color}`)
    };

    submit(e) {
        const {_title, _color} = this.refs;
        e.preventDefault();
        // alert(`New color: ${_title.value} ${_color.value}`);
        this.props.onNewColor(_title.value, _color.value);
        _title.value = '';
        _color.value = '#000000';
        _title.focus();
    }

    render() {
        return (
//            <form onSubmit={e => e.preventDefault()}>
            <form onSubmit={this.submit}>
                <input
                    ref="_title"
                    type="text"
                    placeholder="color title..."
                    required/>
                <input
                    ref="_color"
                    type="color"
                    required/>
                <button>ADD</button>
            </form>
        );
    }
}

export default AddColorForm;