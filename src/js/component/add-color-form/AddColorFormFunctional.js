import React from 'react';
import PropTypes from 'prop-types';

const onNewColorLog = (title, color) => console.log(`[const func] New color: ${title} | ${color}`);

const AddColorFormFunctional = ({onNewColor}) => {
    let _title, _color;
    const submit = e => {
        e.preventDefault();
        onNewColor(_title.value, _color.value);
        _title.value = '';
        _color.value = '#000000';
        _title.focus();
    };

    return (
        <form onSubmit={submit} >
            <input ref={input => _title = input}
                   type="text"
                   placeholder="color title..."
                   required />
            <input ref={input => _color = input}
                   type="color"
                   required />
            <button>ADD</button>
        </form>
    );
};

AddColorFormFunctional.propTypes = {
    onNewColor: PropTypes.func.isRequired
};

AddColorFormFunctional.defaultProps = {
    onNewColor: (title, color) => console.log(`[defaultProp] New color: ${title} | ${color}`)
};

export default AddColorFormFunctional;