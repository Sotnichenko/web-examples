import React from 'react';
import StarRatingNew from "../star-rating/StarRatingNew";

const Color = ({title, color, rating = 0}) =>
    <section className="color" >
        <h1>{title}</h1>
        <div className="color"
             style={{backgroundColor: color}}>
        </div>
        <div>
            <StarRatingNew starsSelected={rating}/>
        </div>
    </section>;

export default Color;