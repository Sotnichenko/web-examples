import React, {Component} from 'react';
import '../../../css/component/easel/Cell.css';

class Cell extends Component {

    render() {
        return(
            <td
                className="cell"
                id={"x" + this.props.y + "x" + this.props.x}
                style={{backgroundColor: this.props.color}}
                onClick={this.props.onClick}
            >
            </td>
        );
    }
}

export default Cell;
