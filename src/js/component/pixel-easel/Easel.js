import React, {Component} from 'react';
import Cell from './Cell'
import CellModel from '../../model/Cell'
import '../../../css/component/easel/Easel.css'
import PropTypes from 'prop-types';

class Easel extends Component {

    constructor(props) {
        super(props);
        this.cells = this.initCells(this.props.xPixels, this.props.yPixels);
        this.state = {
            xPixels: this.props.xPixels,
            yPixels: this.props.yPixels,
            cellSize: this.props.cellSize,
            cells: this.cells,
            brushColor: "#000"
        };
    }

    static propTypes = {
        xPixels: PropTypes.number.isRequired,
        yPixels: PropTypes.number.isRequired,
        cellSize: PropTypes.number.isRequired
    };

    static defaultProps = {
        xPixels: 32,
        yPixels: 18,
        cellSize: 10
    };

    initCells = (xPixels, yPixels) => {
        let cells = Array(yPixels);
        for (let i = 0; i < yPixels; i++) {
            cells[i] = Array(xPixels);
            for (let j = 0; j < xPixels; j++) {
                cells[i][j] = new CellModel(j, i);
            }
        }
        return cells;
    };

    cellClickHandler = event => {
        let cellPosition = event.target.id.split('x');
        cellPosition.shift();
        let [y, x] = cellPosition;
        this.cells[y][x].color = this.state.brushColor;
        this.setState({cells: this.cells});
    };

    cellsAsTable = () => {
        return (
            this.state.cells.map((r, i) => {
                return (
                    <tr key={i}>
                        {r.map((c, j) => {
                            console.log("updating ui");
                            let {x, y, size, color} = c;
                           return(
                               <Cell
                                   key={j}
                                   x={x}
                                   y={y}
                                   color={color}
                                   onClick={this.cellClickHandler}
                               />
                           );
                        })}
                    </tr>
                )
            })
        );
    };

    render() {
        return(
            <div className="border">
                <table id="easel">
                    <tbody>
                        {this.cellsAsTable()}
                    </tbody>
                </table>
                <div id="palette">

                </div>
                <input type="color" id="color-input"/>
                <div className="inline-block" id="current-color">Current color</div>
            </div>
        );
    }
}

export default Easel;
