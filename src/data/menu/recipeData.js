const data = [
    {
        name: "Baked Salmon",
        ingredients: [
            {name: "Salmon", amount: 1, measurement: "1 lb"},
            {name: "Pine Nuts", amount: 1, measurement: "cup"},
            {name: "Butter Letuce", amount: 2, measurement: "cups"},
        ],
        steps: [
            "Preheat the oven to 350 degrees.",
            "Spread the olive oil around a glass baking dish."
        ]
    },
    {
        name: "Fish Tacos",
        ingredients: [
            {name: "Whitefish", amount: 1, measurement: "1 lb"},
            {name: "Cheese", amount: 1, measurement: "cup"},
            {name: "Iceberg Lettuce", amount: 2, measurement: "cups"},
        ],
        steps: [
            "Cook the fish on the grill until hot.",
            "Ploce the fish on the 3 tortillas."
        ]
    }
];

export default data;
