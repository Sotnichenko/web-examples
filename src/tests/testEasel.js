const url = 'http://localhost:8081';

const hexToRgb = hex => {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
    } : null;
};

const rgbToString = hex => {
    let rgb = hexToRgb(hex);
    return rgb ?
        'rgb(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')'
        : null;
};

module.exports = {

    before: function(browser) {
        console.log("Launching browser...");
    },

    after: function(browser) {
        console.log("Closing browser...");
    },

    beforeEach: function(browser) { },

    afterEach: function(browser, done) { done(); },

    // 'Test 1': function(client) {
        // client.url(url)
        //     .waitForElementVisible('body', 1000)
        //     .click('#x14x14');
            // .setValue('input[type=text]', 'nigthwatch')
            // .waitForElementVisible('button[value="Поиск в Google" class="lsb"]', 1000)
            // .click('button[value="Поиск в Google" class="lsb"]')
            // .pause()
            // .assert.containsText('#main', "Night Watch")
            // .end();
            // .done();
    // },
    'Cell color is changed after clicking with another color': function(client) {
        client.url(url);

        client.expect.element("#x7x7").to.be.present.before(1000);

        client.expect.element('#x7x7').to.have.css('background-color')
            .which.contains(rgbToString("#FFFFFF"));

        client.click('#x7x7');

        client.expect.element('#x7x7').to.have.css('background-color')
            .which.contains(rgbToString("#000000"));

        client.end();
    }
};
